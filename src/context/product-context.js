import React, { createContext } from "react";

const CartContext = React.createContext({
    items: [],
    totalQuantity: 0,
    totalAmount: 0,
    addItem: (item) => {
        
    },
    removeItem: (id) => {}
});

export default CartContext;
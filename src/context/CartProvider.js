import React , {useReducer} from "react";
import CartContext from "./product-context";



const defaultCartState = {
    items: [],
    totalQuantity: 0,
    totalAmount: 0,
}

const cartReducer = (state , action) => {
    if(action.type === 'ADD'){
        const updateItems = state.items.concat(action.item);
    }
    return defaultCartState;
}

const CartProvider = props => {

    const [cartState , dispatchCartState] = useReducer(cartReducer , defaultCartState);

    const addItemToCartHandler = item => {
        dispatchCartState({type: "ADD" , item: item});
    };
    const removeItemFromCartHandler = id => {
        dispatchCartState({type: "REMOVE" , id: id});
    };

    const cartContext = {
        items: cartState.items,
        totalQuantity: cartState.totalQuantity,
        totalAmount: cartState.totalAmount,
        addItem: addItemToCartHandler,
        removeItem: removeItemFromCartHandler,
    }
    return(
        <CartContext.Provider value={cartContext}>
            {props.children}
        </CartContext.Provider>
    )
}

export default CartProvider;
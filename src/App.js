import React , {useContext , useState} from "react";
import ProductList from "./component/product/ProductList";
import Header from "./component/Layout/Header";
import CartContext from "./context/product-context";

import Test from "./component/Test/Test";
import AuthContext from "./component/store/auth-context";
import AvalibaleMeals from "./component/Meals/AvalibaleMeals";

function App() {
  // const ctx = useContext(productContext);
  // console.log(ctx)

  const [cartItem , setCartItem] = useState();
  return (
    <React.Fragment>
      <Header />
      <div className="cardWrapper">
        <CartContext.Consumer>
          {ctx => {console.log(ctx.items)}}
        </CartContext.Consumer>
      </div>
      <AvalibaleMeals />
    </React.Fragment>    
  );
}

export default App;

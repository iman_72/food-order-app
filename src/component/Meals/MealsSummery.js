import React , {useRef , useState} from "react";
import Card from "../UI/Card/Card";
import MealsItemForms from "./MealsItemForms";
import AddToCart from "../UI/Button/AddToCart";

const MealsSummery = props => {

   const amountValueHandler = event => {
    console.log(event)
   }
    
    return(
        <Card>
            <h2>{props.name}</h2>
            <div>
                <p>{props.description}</p>
            </div>
            <div>
                <span>{props.price}</span>
            </div>
            <AddToCart />
           <MealsItemForms onAddToCart={amountValueHandler}/>
        </Card>
    )
}

export default MealsSummery;
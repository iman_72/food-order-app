import React from "react";
import Meals from "./Meals";

const AvalibaleMeals = props => {

    const Dummy_Food = [
        {
            id: 1,
            name: 'Joje-Kabab',
            description: 'This is a fake description',
            price: 10,
        },
        {
            id: 2,
            name: 'Kobideh',
            description: 'This is a fake description',
            price: 20,
        },
        {
            id: 3,
            name: 'Zereshk-Polo',
            description: 'This is a fake description',
            price: 8,
        },
        {
            id: 4,
            name: 'Ghormeh',
            description: 'This is a fake description',
            price: 12,
        },
    ]

    return(
        <Meals products={Dummy_Food} />
    )
}

export default AvalibaleMeals;
import React , {useRef , useState} from "react";
import Input from "../UI/Input/Input";


const MealsItemForms = props => {
    const amountInputRef = useRef();
    const [amountIsValid , setAmountIsValid] = useState(true);

    const addProductHandler = event =>{
        event.preventDefault()
        const entredAmount =  amountInputRef.current.value;
        const entredAmountNumber = +amountInputRef.current.value;

        if(entredAmount.trim().length === 0 || entredAmountNumber < 1 || entredAmountNumber > 5){
            setAmountIsValid(false);
            return;
        }

        props.onAddToCart(entredAmountNumber);
    }

    return(
        <React.Fragment>
             <form onSubmit={addProductHandler}>
                <Input
                 ref={amountInputRef}
                 lable='Amount'
                 input={{
                    id: 'amount',
                    type: 'number',
                    min: 1,
                    max: 5,
                    step: 1,
                    defaultValue: 1,
                 }}/>
                 
            </form>
            <div>{!amountIsValid && <p>Pleas Enter a Valid Number (1-5)</p>}</div>
        </React.Fragment>
    )

}

export default MealsItemForms;
import React from "react";
import MealsSummery from "./MealsSummery";

const Meals = props => {
    props.products.map(item => {
        // console.log(item)
    })
    
    return(
        <div className="ih">
            {props.products.map(item => (
                <MealsSummery 
                    key={item.id}
                    id={item.id}
                    name={item.name}
                    description={item.description}
                    price={item.price}
                />
            ))}
        </div>
    )
}

export default Meals;

import styles from "./Header.module.css";

const Header = () => {
    return(
        <header>
            <div>
                <h1>ReactMeals</h1>
                <button>Cart</button>
                <span>0</span>
            </div>
        </header>
    )
}

export default Header;
import React from "react";
import AuthContext from "../store/auth-context";

const Test = props => {
    return(
        <div>
            <div>Hello Context</div>
            <div>Context can make you Master!</div>
            <AuthContext.Consumer>
                {(ctx) => {
                    console.log(ctx.inLoggedIn)
                }}
            </AuthContext.Consumer>
        </div>
    )
}

export default Test;
import React from "react";
import styles from "./AddToCart.module.css"

const AddToCart = props => {
    return(
        <input type='submit' value="Add To Carts" onClick={props.onClick} className={styles.addToCart}/>
    )
}

export default AddToCart;
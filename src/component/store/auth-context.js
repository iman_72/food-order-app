import React from "react";

const AuthContext = React.createContext({
    inLoggedIn: false,
})

export default AuthContext;